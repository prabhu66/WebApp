<html>
<head>
<title>Cloverleaf!!</title>
<link rel="stylesheet" href="main.css">
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<div class="dashboardContent">
<p class="title">Welcome CloverLeaf${name} </p><br>
	<form action="reports.jsp" method="POST" id="dashboardForm">
	<table>
        <tr><td class="tableColumn1">Account Name </td>
        <td class="tableColumn2"><select name="accountName" id="accountName" class="tableColumn2Select">
        	<option>Atlanta Beverages</option>
        	<option>Jean Coutu</option>
        </select></td>
        </tr>
        <tr><td class="tableColumn1">Process </td>
       <td class="tableColumn2"><select name="processName" id="processName" class="tableColumn2Select">
        	<option>Azure Table to Blob</option>
        	<option>ETL</option>
        	<option>SQL Server to Spark</option>
        	<option>PowerBI Report</option>
        </select>  </td>
        </tr>
       <tr><td colspan="2"> <input type="submit" value="start"> </td></tr>
        </table>
    </form>
    <div id="statusDiv"></div>
   </div>
 <!--  <script>
	$( "#dashboardForm" ).submit(function( event ) {
	  // alert( $("#processName").val() + " process started" );
	  var processName = $("#processName").val();
	  var accountName =$("#accountName").val();
	//  action="./login.htm" method="POST"
	$("#statusDiv").html("<p>" + processName +" for " + $("#accountName").val() + " is in Progress</p>" );
	 event.preventDefault();
	 $.ajax({
		  type: "POST",
		  url: './dashboard.htm',
		  data: {
				accountName:$("#accountName").val(),
				processName:$("#processName").val()
			},
		  success: function(data){
			  //alert("data: "+data);
			if(processName == "PowerBI Report"){
				$("#statusDiv").html("<p>" + processName +" for " + accountName + " is Completed</p>" );
				window.open(data);	
			}else{
				// alert(processName + ' process completed');
				$("#statusDiv").html("<p>" + processName +" for " + accountName + " is Completed</p>" );
			}  
		  },
		  error: function(textStatus, errorThrown){
			$("#statusDiv").html("<p>" + processName +" for " + accountName + " is Failed</p>" );
		  }
		});
	});
</script>  -->
</body>

</html>