<%
	String accountName = request.getParameter("accountName");
	session.setAttribute("accountName", accountName);
    String processName = request.getParameter("processName");
	session.setAttribute("processName", processName);
%>
<html>
<head>
<title>Cloverleaf!!</title>
<link rel="stylesheet" href="main.css">
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>

	<form>
		<input type="hidden" name="accountName" id="accountName" value="${sessionScope.accountName}" />
		<input type="hidden" name="processName" id="processName" value="${sessionScope.processName}" />
	</form>
 <div id="statusDiv"></div>

<script>
	$( "document" ).ready(function() { 
	  // alert( $("#processName").val() + " process started" );
	  var processName = $("#processName").val();
	  var accountName =$("#accountName").val();
	//  action="./login.htm" method="POST"
	$("#statusDiv").html("<p>" + processName +" for " + accountName + " is in Progress</p>" );
	// event.preventDefault();
	$.ajax({
	  type: "POST",
	  url: './dashboard.htm',
	  data: {
			accountName:$("#accountName").val(),
			processName:$("#processName").val()
		},
	  success: function(data){
		  //alert("data: "+data);
		if(processName == "PowerBI Report"){
			$("#statusDiv").html("<p>" + processName +" for " + accountName + " is Completed</p>" );
			window.open(data);	
		}else{
			// alert(processName + ' process completed');
			$("#statusDiv").html("<p>" + processName +" for " + accountName + " is Completed</p>" );
		}  
	  },
	  error: function(textStatus, errorThrown){
		$("#statusDiv").html("<p>" + processName +" for " + accountName + " is Failed</p>" );
	  }
	});
/*	$.post('./dashboard.htm',{
		accountName:$("#accountName").val(),
		processName:$("#processName").val()
	},function(err,data){
		 alert('${result}');
		  alert("data: "+data);
		  alert("err: "+err);
		if(err){
			$("#statusDiv").html("<p>" + processName +" for " + accountName + " is Failed</p>" );
		}
		else if(processName == "PowerBI Report"){
			window.open(data);	
		}else{
			// alert(processName + ' process completed');
			$("#statusDiv").html("<p>" + processName +" for " + accountName + " is Completed</p>" );
		}

	}) */
	});
</script> 
<form action="welcome.jsp" id="back">

 <p><input type="submit" value="Back"/></p>
</form>
</body>
</html>