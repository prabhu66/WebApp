package com.cloverleaf;


	
	import java.text.DateFormat;
	import java.text.SimpleDateFormat;
	import java.util.Date;
	import java.util.UUID;

	import com.microsoft.azure.storage.table.TableServiceEntity;

	public class EmotionTrackingData extends TableServiceEntity {
		private String partitionKey;
		private String rowKey;
		private Date timestamp;
		private UUID eventId;
		private String eventTimestamp;
		private String startTime;
		private String endTime;
		private String activeStates;
		private String created;
		private String modified;
		private String eventData;
		private String value;
		public String getPartitionKey() {
			return partitionKey;
		}
		public void setPartitionKey(String partitionKey) {
			this.partitionKey = partitionKey;
		}
		public String getRowKey() {
			return rowKey;
		}
		public void setRowKey(String rowKey) {
			this.rowKey = rowKey;
		}
		public Date getTimestamp() {
			return timestamp;
		}
		public void setTimestamp(Date timestamp) {
			this.timestamp = timestamp;
		}
		public UUID getEventId() {
			return eventId;
		}
		public void setEventId(UUID eventId) {
			this.eventId = eventId;
		}
		public String getEventTimestamp() {
			return eventTimestamp;
		}
		public void setEventTimestamp(String eventTimestamp) {
			this.eventTimestamp = eventTimestamp;
		}
		public String getStartTime() {
			return startTime;
		}
		public void setStartTime(String startTime) {
			this.startTime = startTime;
		}
		public String getEndTime() {
			return endTime;
		}
		public void setEndTime(String endTime) {
			this.endTime = endTime;
		}
		public String getActiveStates() {
			return activeStates;
		}
		public void setActiveStates(String activeStates) {
			this.activeStates = activeStates;
		}
		public String getCreated() {
			return created;
		}
		public void setCreated(String created) {
			this.created = created;
		}
		public String getModified() {
			return modified;
		}
		public void setModified(String modified) {
			this.modified = modified;
		}
		public String getEventData() {
			return eventData;
		}
		public void setEventData(String eventData) {
			this.eventData = eventData;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		
		
			
		
	}



