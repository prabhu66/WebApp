package com.cloverleaf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableQuery;
import com.microsoft.azure.storage.table.TableQuery.Operators;
import com.microsoft.azure.storage.table.TableQuery.QueryComparisons;

public class ProcessService {
	static{
		try{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Class.forName("org.apache.hive.jdbc.HiveDriver");
			
			}catch(Exception e){}
		
	}
	private String result;
	private Map<String, String> tableNames;

	private static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	private static Date date = new Date();
	final static String ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	final static SimpleDateFormat sdf = new SimpleDateFormat(ISO_FORMAT);
	final static TimeZone utc = TimeZone.getTimeZone("UTC");
	static {
		sdf.setTimeZone(utc);
	}
	static Logger log = Logger.getLogger(LoginController.class.getName());
	public String getReport(String accountName, String processName) {
		log.info("starting process");

		if (accountName.equals("Atlanta Beverages") && processName.equals("Azure Table to Blob")) {
			log.info("starting abTableToBlobEmotionTrackingData");
			result = abTableToBlobEmotionTrackingData();
			result = result + "\n" + abTableToBlobShopperTrackingData();
			
			log.info("completed abTableToBlobEmotionTrackingData");

		} else if (accountName.equals("Atlanta Beverages") && processName.equals("ETL")) {
			log.info("starting abETL");
			result = abETL();
			
			log.info("completed abETL");
		}

		else if (accountName.equals("Atlanta Beverages") && processName.equals("SQL Server to Spark")) {
			log.info("starting abSqlToSpark");
			tableNames = new HashMap<String, String>();
			tableNames.put("Emotion",
					"CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.Emotion\r\n" + "(\r\n"
							+ "    EventId string,\r\n" + "    EmotionName string,\r\n" + "    EmotionValue string,\r\n"
							+ "    Source string\r\n" + ")\r\n"
							+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"
							+ "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/Emotion'");

//			tableNames.put("Product",
//					"CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.Product\r\n" + "(\r\n"
//							+ "    EventId string,\r\n" + "    InventoryTrackingNumber string,\r\n"
//							+ "    InventoryTrackingNumberType string,\r\n" + "    ProductName string,\r\n"
//							+ "    Source string,\r\n" + "    Brand string\r\n" + ")\r\n"
//							+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"
//							+ "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/Product'");
//
//			tableNames.put("EventData",
//					"CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.EventData\r\n" + "(\r\n"
//							+ "    EventId string,\r\n" + "    AgeRange string,\r\n" + "    CompanyType string,\r\n"
//							+ "    ConnectorType string,\r\n" + "    ContentDisplayEvent string,\r\n"
//							+ "    Customer string,\r\n" + "    Emotion string,\r\n" + "    Expression string,\r\n"
//							+ "    FaceQualityName string,\r\n" + "    FaceQualityValue string,\r\n"
//							+ "    Gender string,\r\n" + "    Products string,\r\n" + "    Race string,\r\n"
//							+ "    Region string,\r\n" + "    SensorRegistry string,\r\n" + "    SensorType string,\r\n"
//							+ "    State string,\r\n" + "    StockLevel string,\r\n" + "    Unit string,\r\n"
//							+ "    UnitType string,\r\n" + "    Source string\r\n" + ")\r\n"
//							+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"
//							+ "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/EventData'");
//
			tableNames.put("Expression",
					"CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.Expression\r\n" + "(\r\n"
							+ "    EventId string,\r\n" + "    ExpressionName string,\r\n"
							+ "    ExpressionValue string,\r\n" + "    Source string   \r\n" + ")\r\n"
							+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"
							+ "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/Expression'");

//			tableNames.put("ShopperData", "CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.ShopperData\r\n"
//					+ "(\r\n" + "    EventId string,\r\n" + "    Company string,\r\n" + "    CompanyType string,\r\n"
//					+ "    Connectortype string,\r\n" + "    SensorRegistry string,\r\n" + "    SensorType string,\r\n"
//					+ "    EngagedShoppersCount string,\r\n" + "    ShoppersCount string,\r\n"
//					+ "    StoreName string,\r\n" + "    StoreProvince string,\r\n" + "    UnitName string,\r\n"
//					+ "    UnitType string,\r\n" + "    Source string   \r\n" + ")\r\n"
//					+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"
//					+ "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/ShopperData'");
//
//			/*
//			 * tableNames.put("ShopperTrackingData",
//			 * "CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.ShopperTrackingData\r\n"
//			 * + "(\r\n" + "    PartitionKey string,\r\n" + "    RowKey string,\r\n" +
//			 * "    SensorRegistryId string,\r\n" + "    EventId string,\r\n" +
//			 * "    Created string,\r\n" + "    Value string,\r\n" +
//			 * "    Modified string,\r\n" + "    Timestamp datetime\r\n" + ")\r\n" +
//			 * "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"+
//			 * "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/ShopperTrackingData'"
//			 * );
//			 */

			tableNames.put("Sales_YTD",
					"CREATE EXTERNAL TABLE if not exists AbJc.Sales_YTD\r\n" + "(\r\n"
							+ "    RetailAccounts string,\r\n" + "    ADDR string,\r\n" + "    CITY string,\r\n"
							+ "    WeekNumber string,\r\n" + "    CaseEquivsAmount float,\r\n" + "    Brand string,\r\n"
							+ "    year int,\r\n" + "    ACCT int \r\n" + ")\r\n"
							+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"
							+ "STORED AS TEXTFILE LOCATION 'wasb:///AbJc/Sales_YTD'");

			tableNames.put("Stores_Sales",
					"CREATE EXTERNAL TABLE if not exists AbJc.Stores_Sales\r\n" + "(\r\n"
							+ "    StoreGroup string,\r\n" + "    SaleType string,\r\n" + "    WeekNumber int,\r\n"
							+ "    CalenderYear int,\r\n" + "    Sales int \r\n" + ")\r\n"
							+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"
							+ "STORED AS TEXTFILE LOCATION 'wasb:///AbJc/Stores_Sales'");

			for (Map.Entry<String, String> m : tableNames.entrySet()) {
				log.info(m.getKey() + " _____ " + m.getValue());
				result = result + "\n" + abSqlToSpark(m.getKey(), m.getValue());
			}
			log.info("completing abSqlToSpark");

		}

		else if (accountName.equals("Atlanta Beverages") && processName.equals("PowerBI Report")) {
			log.info("starting abPowerBI");
			result = abPowerBI();
			log.info("completing abPowerBI");
		}

		else if (accountName.equals("Jean Coutu") && processName .equals("Azure Table to Blob")) {
			result = jcTableToBlobEmotionTrackingData();
			result = result + "\n" + jcTableToBlobShopperTrackingData();
		}

		else if (accountName.equals("Jean Coutu") && processName.equals("ETL")) {
			result = jcETL();
		}

		else if (accountName.equals("Jean Coutu") && processName.equals("SQL Server to Spark")) {
			tableNames = new HashMap<String, String>();
			tableNames.put("Emotion",
					"CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.Emotion\r\n" + "(\r\n"
							+ "    EventId string,\r\n" + "    EmotionName string,\r\n" + "    EmotionValue string,\r\n"
							+ "    Source string\r\n" + ")\r\n"
							+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"
							+ "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/Emotion'");

			tableNames.put("Product",
					"CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.Product\r\n" + "(\r\n"
							+ "    EventId string,\r\n" + "    InventoryTrackingNumber string,\r\n"
							+ "    InventoryTrackingNumberType string,\r\n" + "    ProductName string,\r\n"
							+ "    Source string,\r\n" + "    Brand string\r\n" + ")\r\n"
							+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"
							+ "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/Product'");

			tableNames.put("EventData",
					"CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.EventData\r\n" + "(\r\n"
							+ "    EventId string,\r\n" + "    AgeRange string,\r\n" + "    CompanyType string,\r\n"
							+ "    ConnectorType string,\r\n" + "    ContentDisplayEvent string,\r\n"
							+ "    Customer string,\r\n" + "    Emotion string,\r\n" + "    Expression string,\r\n"
							+ "    FaceQualityName string,\r\n" + "    FaceQualityValue string,\r\n"
							+ "    Gender string,\r\n" + "    Products string,\r\n" + "    Race string,\r\n"
							+ "    Region string,\r\n" + "    SensorRegistry string,\r\n" + "    SensorType string,\r\n"
							+ "    State string,\r\n" + "    StockLevel string,\r\n" + "    Unit string,\r\n"
							+ "    UnitType string,\r\n" + "    Source string\r\n" + ")\r\n"
							+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"
							+ "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/EventData'");

			tableNames.put("Expression",
					"CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.Expression\r\n" + "(\r\n"
							+ "    EventId string,\r\n" + "    ExpressionName string,\r\n"
							+ "    ExpressionValue string,\r\n" + "    Source string   \r\n" + ")\r\n"
							+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"
							+ "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/Expression'");

			tableNames.put("ShopperData", "CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.ShopperData\r\n"
					+ "(\r\n" + "    EventId string,\r\n" + "    Company string,\r\n" + "    CompanyType string,\r\n"
					+ "    Connectortype string,\r\n" + "    SensorRegistry string,\r\n" + "    SensorType string,\r\n"
					+ "    EngagedShoppersCount string,\r\n" + "    ShoppersCount string,\r\n"
					+ "    StoreName string,\r\n" + "    StoreProvince string,\r\n" + "    UnitName string,\r\n"
					+ "    UnitType string,\r\n" + "    Source string   \r\n" + ")\r\n"
					+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"
					+ "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/ShopperData'");

			/*
			 * tableNames.put("ShopperTrackingData",
			 * "CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.ShopperTrackingData\r\n"
			 * + "(\r\n" + "    PartitionKey string,\r\n" + "    RowKey string,\r\n" +
			 * "    SensorRegistryId string,\r\n" + "    EventId string,\r\n" +
			 * "    Created string,\r\n" + "    Value string,\r\n" +
			 * "    Modified string,\r\n" + "    Timestamp datetime\r\n" + ")\r\n" +
			 * "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"+
			 * "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/ShopperTrackingData'"
			 * );
			 */

			tableNames.put("Sales_YTD",
					"CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.Sales_YTD\r\n" + "(\r\n"
							+ "    RetailAccounts string,\r\n" + "    ADDR string,\r\n" + "    CITY string,\r\n"
							+ "    WeekNumber string,\r\n" + "    CaseEquivsAmount float,\r\n" + "    Brand string,\r\n"
							+ "    year int,\r\n" + "    ACCT int \r\n" + ")\r\n"
							+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"
							+ "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/Sales_YTD'");

			tableNames.put("Stores_Sales",
					"CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.Stores_Sales\r\n" + "(\r\n"
							+ "    StoreGroup string,\r\n" + "    SaleType string,\r\n" + "    WeekNumber int,\r\n"
							+ "    CalenderYear int,\r\n" + "    Sales int \r\n" + ")\r\n"
							+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"
							+ "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/Stores_Sales'");

			for (Map.Entry<String, String> m : tableNames.entrySet()) {
				log.info(m.getKey() + " _____ " + m.getValue());
				result = result + "\n" + jcSqlToSpark(m.getKey(), m.getValue());
			}

		}

		else if (accountName.equals("Jean Coutu") && processName.equals("PowerBI Report")) {
			result = jcPowerBI();
		}
		log.info("result: "+result);
		return result;
	}

	public String abTableToBlobEmotionTrackingData() {
		try {
			final String storageConnectionStringSource = "DefaultEndpointsProtocol=http;"
					+ "AccountName=atlbevdefaultstorage;"
					+ "AccountKey=BFlGPZIjifWPUEatWGg1fYgHnng3GG6TVkQBmiCVpDtFJtwgnq4UBPaNTv5QIoAKx1mIZIgGP5ZonwBvb4YXKg==";

			final String storageConnectionStringDestination = "DefaultEndpointsProtocol=https;"
					+ "AccountName=cloverleafsparkstorage;"
					+ "AccountKey=VMphLcZkEOlxSdfB+0guQsYqmnAuN336OnA0IYXM9jixPuCrnZ9COKj6WU0QAkI4RbfzR3VeMxUFEH2m8pilSg==";

			// Delimiter used in CSV file
			final String COMMA_DELIMITER = ",";
			final String NEW_LINE_SEPARATOR = "\n";

			// CSV file header
			final String FILE_HEADER = "PartitionKey,RowKey,Timestamp,EventId,EventTimestamp,StartTime,EndTime,ActiveStates,Created,Modified,EventData,Value";

			// Define constants for filters.
			final String PARTITION_KEY = "PartitionKey";
			final String ROW_KEY = "RowKey";
			final String TIMESTAMP = "Timestamp";

			// Retrieve storage account from connection-string.
			CloudStorageAccount storageAccountSource = CloudStorageAccount.parse(storageConnectionStringSource);

			// Create the table client.
			CloudTableClient tableClient = storageAccountSource.createCloudTableClient();

			// Create a cloud table object for the table.
			CloudTable cloudTable = tableClient.getTableReference("emotiontrackingdata");

			// Create a filter condition where the partition key is "emotiontrackingEvent".
			String timeStampFilter = TableQuery.generateFilterCondition(TIMESTAMP, QueryComparisons.GREATER_THAN,
					new Date());

			String rowFilter = TableQuery.generateFilterCondition(ROW_KEY, QueryComparisons.EQUAL,
					"008f2e49-4c7c-4ce2-b067-07bf81a8a30b");

			String partitionFilter = TableQuery.generateFilterCondition(PARTITION_KEY, QueryComparisons.EQUAL,
					"EmotionTrackingEvent");

			String combinedFilter = TableQuery.combineFilters(partitionFilter, Operators.OR, timeStampFilter);

			// Specify a partition query, using "Smith" as the partition key filter.
			TableQuery<EmotionTrackingData> partitionQuery = TableQuery.from(EmotionTrackingData.class);

			FileWriter fileWriter = null;
			JSONParser parser = new JSONParser();

			try {
				fileWriter = new FileWriter("C:/Users/Public/CloverLeaf Files/EmotionTrackingData" + dateFormat.format(date) + ".csv");

				// Write the CSV file header
				fileWriter.append(FILE_HEADER.toString());

				// Add a new line separator after the header
				fileWriter.append(NEW_LINE_SEPARATOR);

				int count = 0;
//				Calendar myCalendar = new GregorianCalendar(2018, 0, 4);
//				Date myDate = myCalendar.getTime();
//				//Date myDate =new Date();
//				log.info("mydate : "+myDate+" :::: "+new Date());

				// Checking config file for flag

				Object obj = parser.parse(new FileReader("C:/Users/Public/CloverLeaf Files/abTabletoBlobEmotionTrackingDataconfig.json"));

				JSONObject jsonObject = (JSONObject) obj;
	            long flag = (Long) jsonObject.get("flag");
	            String dateStr = (String) jsonObject.get("timestamp");
	            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	            Date mydate = sdf.parse(dateStr);
	            log.info(mydate);
				if (flag == 1) {
					String endDate = dateStr;
					// Loop through the results, displaying information about the entity.
					for (EmotionTrackingData entity : cloudTable.execute(partitionQuery)) {
						if ((entity.getTimestamp()).after(mydate)) {
							log.info((count++) + "\t" + entity.getPartitionKey() + "\t" + entity.getRowKey()
									+ "\t" + sdf.format(entity.getTimestamp()) + "\t" + entity.getRowKey() + "\t"
									+ entity.getEventTimestamp() + "\t" + entity.getStartTime() + "\t"
									+ entity.getEndTime() + "\t" + entity.getActiveStates() + "\t" + entity.getCreated()
									+ "\t" + entity.getModified() + "\t" + entity.getEventData() + "\t"
									+ entity.getValue()

							);

							fileWriter.append(String.valueOf(entity.getPartitionKey()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getRowKey()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(sdf.format(entity.getTimestamp())));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getRowKey()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getEventTimestamp()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getStartTime()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getEndTime()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(
									"\"" + (String.valueOf(entity.getActiveStates())).replace("\"", "'") + "\"");
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getCreated()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getModified()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append("\"" + String.valueOf(entity.getEventData()).replace("\"", "'") + "\"");
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append("\"" + String.valueOf(entity.getValue()).replace("\"", "'") + "\"");

							fileWriter.append(NEW_LINE_SEPARATOR);
						}
						if((entity.getTimestamp()).after(sdf.parse(endDate))) {
				    		endDate = sdf.format(entity.getTimestamp());
				    	}
				    	
		            }
				    FileWriter fileWriter1 = new FileWriter("C:/Users/Public/CloverLeaf Files/abTabletoBlobEmotionTrackingDataconfig.json");
		            JSONObject configObj = new JSONObject();
		            configObj.put("flag",1);
		            configObj.put("timestamp", endDate);
		            log.info("\n end date: " + endDate);
		            fileWriter1.write(configObj.toJSONString());
		            fileWriter1.close();
				}
				if (flag == 0) {
					String endDate = sdf.format(new Date());
					// Loop through the results, displaying information about the entity.
					for (EmotionTrackingData entity : cloudTable.execute(partitionQuery)) {

						log.info((count++) + "\t" + entity.getPartitionKey() + "\t" + entity.getRowKey()
								+ "\t" + sdf.format(entity.getTimestamp()) + "\t" + entity.getRowKey() + "\t"
								+ entity.getEventTimestamp() + "\t" + entity.getStartTime() + "\t" + entity.getEndTime()
								+ "\t" + entity.getActiveStates() + "\t" + entity.getCreated() + "\t"
								+ entity.getModified() + "\t" + entity.getEventData() + "\t" + entity.getValue()

						);

						fileWriter.append(String.valueOf(entity.getPartitionKey()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getRowKey()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(sdf.format(entity.getTimestamp())));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getRowKey()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getEventTimestamp()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getStartTime()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getEndTime()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append("\"" + (String.valueOf(entity.getActiveStates())).replace("\"", "'") + "\"");
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getCreated()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getModified()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append("\"" + String.valueOf(entity.getEventData()).replace("\"", "'") + "\"");
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append("\"" + String.valueOf(entity.getValue()).replace("\"", "'") + "\"");

						fileWriter.append(NEW_LINE_SEPARATOR);
						if(count == 1) {
							endDate = sdf.format(entity.getTimestamp());
						}
						else {
							if((entity.getTimestamp()).after(sdf.parse(endDate))) {
					    		endDate = sdf.format(entity.getTimestamp());
					    	}
						}
						
				    }
				    FileWriter fileWriter1 = new FileWriter("C:/Users/Public/CloverLeaf Files/abTabletoBlobEmotionTrackingDataconfig.json");
		            JSONObject configObj = new JSONObject();
		            configObj.put("flag",1);
		            configObj.put("timestamp", endDate);
		            log.info("\n end date: " + endDate);
		            fileWriter1.write(configObj.toJSONString());
		            fileWriter1.close();
				}

				log.info(count + " records fetched emotiontrackingdata");

				log.info("emotiontrackingdata CSV file was created successfully !!!");

			} catch (Exception e) {
				log.info("Error in CsvFileWriter !!!");
				e.printStackTrace();
			} finally {

				try {
					fileWriter.flush();
					fileWriter.close();

					File sourceFile = new File("C:/Users/Public/CloverLeaf Files/EmotionTrackingData" + dateFormat.format(date) + ".csv");
					log.info("File Name : EmotionTrackingData" + dateFormat.format(date) + ".csv");

					CloudStorageAccount storageAccountDestination;
					CloudBlobClient blobClient = null;
					CloudBlobContainer container = null;

					// Parse the connection string and create a blob client to interact with Blob
					// storage
					storageAccountDestination = CloudStorageAccount.parse(storageConnectionStringDestination);
					blobClient = storageAccountDestination.createCloudBlobClient();
					container = blobClient.getContainerReference(
							"clfspark-2017-11-08t09-26-18-871z/AtlantaBeveregeStorage/EmotionTrackingData"); // Blob
																												// Name

					// Getting a blob reference
					CloudBlockBlob blob = container
							.getBlockBlobReference("EmotionTrackingData" + dateFormat.format(date) + ".csv");

					// Creating blob and uploading file to it
					log.info("Uploading the EmotionTrackingData CSV file ");
					blob.uploadFromFile(sourceFile.getAbsolutePath());

					// //Listing contents of container

					log.info("Uploaded the EmotionTrackingData CSV file in the Blob");
					// for (ListBlobItem blobItem : container.listBlobs()) {
					// log.info("URI of blob is: " + blobItem.getUri());
					// }

				} catch (IOException e) {
					log.info("Error while flushing/closing fileWriter !!!");
					e.printStackTrace();
				} catch (StorageException ex) {
					System.out
							.println(String.format("Error returned from the service. Http code: %d and error code: %s",
									ex.getHttpStatusCode(), ex.getErrorCode()));
				}
			}
		} catch (Exception e) {
			// Output the stack trace.
			e.printStackTrace();
		}

		return "AtlantaBeverege AzureTabletoBlob EmotionTrackingData Completed Successfully";
	}

	public String abTableToBlobShopperTrackingData() {
		try {

			final String storageConnectionStringSource = "DefaultEndpointsProtocol=http;"
					+ "AccountName=atlbevdefaultstorage;"
					+ "AccountKey=BFlGPZIjifWPUEatWGg1fYgHnng3GG6TVkQBmiCVpDtFJtwgnq4UBPaNTv5QIoAKx1mIZIgGP5ZonwBvb4YXKg==";

			final String storageConnectionStringDestination = "DefaultEndpointsProtocol=https;"
					+ "AccountName=cloverleafsparkstorage;"
					+ "AccountKey=VMphLcZkEOlxSdfB+0guQsYqmnAuN336OnA0IYXM9jixPuCrnZ9COKj6WU0QAkI4RbfzR3VeMxUFEH2m8pilSg==";

			// Delimiter used in CSV file
			final String COMMA_DELIMITER = ",";
			final String NEW_LINE_SEPARATOR = "\n";

			// CSV file header
			final String FILE_HEADER = "PartitionKey,RowKey,Timestamp,EventId,SensorRegistryId,Value,Created,Modified";

			// Define constants for filters.
			final String PARTITION_KEY = "PartitionKey";
			final String ROW_KEY = "RowKey";
			final String TIMESTAMP = "Created";

			// Retrieve storage account from connection-string.
			CloudStorageAccount storageAccountSource = CloudStorageAccount.parse(storageConnectionStringSource);

			// Create the table client.
			CloudTableClient tableClient = storageAccountSource.createCloudTableClient();

			// Create a cloud table object for the table.
			CloudTable cloudTable = tableClient.getTableReference("shoppertrackingdata");

			String timeStampFilter = TableQuery.generateFilterCondition(TIMESTAMP, QueryComparisons.GREATER_THAN,
					new Date());

			String rowFilter = TableQuery.generateFilterCondition(ROW_KEY, QueryComparisons.GREATER_THAN,
					"00355a0c-a40a-49ba-872e-927a7ed24d02");

			String partitionFilter = TableQuery.generateFilterCondition(PARTITION_KEY, QueryComparisons.EQUAL,
					"ShopperTrackingEvent");

			String combinedFilter = TableQuery.combineFilters(partitionFilter, Operators.AND, rowFilter);

			// Specify a partition query, using "Smith" as the partition key filter.
			TableQuery<ShopperTrackingData> partitionQuery = TableQuery.from(ShopperTrackingData.class);

			FileWriter fileWriter = null;
			JSONParser parser = new JSONParser();

			try {
				fileWriter = new FileWriter("C:/Users/Public/CloverLeaf Files/ShopperTrackingData" + dateFormat.format(date) + ".csv");

				// Write the CSV file header
				fileWriter.append(FILE_HEADER.toString());

				// Add a new line separator after the header
				fileWriter.append(NEW_LINE_SEPARATOR);

				int count = 0;
							
				Object obj = parser.parse(new FileReader("C:/Users/Public/CloverLeaf Files/abTabletoBlobShopperTrackingDataconfig.json"));

				JSONObject jsonObject = (JSONObject) obj;
	            long flag = (Long) jsonObject.get("flag");
	            String dateStr = (String) jsonObject.get("timestamp");
	            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	            Date mydate = sdf.parse(dateStr);
	            log.info(mydate);
				if (flag == 1) {
					String endDate = dateStr;
					// Loop through the results, displaying information about the entity.
					for (ShopperTrackingData entity : cloudTable.execute(partitionQuery)) {
						if ((entity.getTimestamp()).after(mydate)) {
							log.info((count++) + "\t" + entity.getPartitionKey() + "\t" + entity.getRowKey()
									+ "\t" + sdf.format(entity.getTimestamp()) + "\t" + entity.getEventId() + "\t"
									+ entity.getSensorRegistryId() + "\t" + entity.getValue() + "\t"
									+ entity.getCreated() + "\t" + entity.getModified());

							fileWriter.append(String.valueOf(entity.getPartitionKey()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getRowKey()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(sdf.format(entity.getTimestamp())));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getEventId()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getSensorRegistryId()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append("\"" + (String.valueOf(entity.getValue())).replace("\"", "'") + "\"");
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getCreated()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getModified()));
							fileWriter.append(NEW_LINE_SEPARATOR);
						}
						if((entity.getTimestamp()).after(sdf.parse(endDate))) {
				    		endDate = sdf.format(entity.getTimestamp());
				    	}
				    	
		            }
				    FileWriter fileWriter1 = new FileWriter("C:/Users/Public/CloverLeaf Files/abTabletoBlobShopperTrackingDataconfig.json");
		            JSONObject configObj = new JSONObject();
		            configObj.put("flag",1);
		            configObj.put("timestamp", endDate);
		            log.info("\n end date: " + endDate);
		            fileWriter1.write(configObj.toJSONString());
		            fileWriter1.close();
				}

				if (flag == 0) {
					String endDate = sdf.format(new Date());
					// Loop through the results, displaying information about the entity.
					for (ShopperTrackingData entity : cloudTable.execute(partitionQuery)) {
						log.info((count++) + "\t" + entity.getPartitionKey() + "\t" + entity.getRowKey()
								+ "\t" + sdf.format(entity.getTimestamp()) + "\t" + entity.getEventId() + "\t"
								+ entity.getSensorRegistryId() + "\t" + entity.getValue() + "\t" + entity.getCreated()
								+ "\t" + entity.getModified()

						);

						fileWriter.append(String.valueOf(entity.getPartitionKey()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getRowKey()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(sdf.format(entity.getTimestamp())));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getEventId()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getSensorRegistryId()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append("\"" + (String.valueOf(entity.getValue())).replace("\"", "'") + "\"");
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getCreated()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getModified()));
						fileWriter.append(NEW_LINE_SEPARATOR);
						if(count == 1) {
							endDate = sdf.format(entity.getTimestamp());
						}
						else {
							if((entity.getTimestamp()).after(sdf.parse(endDate))) {
					    		endDate = sdf.format(entity.getTimestamp());
					    	}
						}
						
				    }
				    FileWriter fileWriter1 = new FileWriter("C:/Users/Public/CloverLeaf Files/abTabletoBlobShopperTrackingDataconfig.json");
		            JSONObject configObj = new JSONObject();
		            configObj.put("flag",1);
		            configObj.put("timestamp", endDate);
		            log.info("\n end date: " + endDate);
		            fileWriter1.write(configObj.toJSONString());
		            fileWriter1.close();

				}

				log.info(count + " records fetched shoppertrackingdata");

				log.info("shoppertrackingdata CSV file was created successfully !!!");

			} catch (Exception e) {
				log.info("Error in CsvFileWriter !!!");
				e.printStackTrace();
			} finally {

				try {
					fileWriter.flush();
					fileWriter.close();

					File sourceFile = new File("C:/Users/Public/CloverLeaf Files/ShopperTrackingData" + dateFormat.format(date) + ".csv");
					log.info("File Name : ShopperTrackingData" + dateFormat.format(date) + ".csv");

					CloudStorageAccount storageAccountDestination;
					CloudBlobClient blobClient = null;
					CloudBlobContainer container = null;

					// Parse the connection string and create a blob client to interact with Blob
					// storage
					storageAccountDestination = CloudStorageAccount.parse(storageConnectionStringDestination);
					blobClient = storageAccountDestination.createCloudBlobClient();
					container = blobClient.getContainerReference(
							"clfspark-2017-11-08t09-26-18-871z/AtlantaBeveregeStorage/ShopperTrackingData");

					// Getting a blob reference
					CloudBlockBlob blob = container
							.getBlockBlobReference("ShopperTrackingData" + dateFormat.format(date) + ".csv");

					// Creating blob and uploading file to it
					log.info("Uploading the ShopperTrackingData CSV file ");
					blob.uploadFromFile(sourceFile.getAbsolutePath());

					// //Listing contents of container
					// for (ListBlobItem blobItem : container.listBlobs()) {
					// log.info("URI of blob is: " + blobItem.getUri());
					// }

				} catch (IOException e) {
					log.info("Error while flushing/closing fileWriter !!!");
					e.printStackTrace();
				} catch (StorageException ex) {
					System.out
							.println(String.format("Error returned from the service. Http code: %d and error code: %s",
									ex.getHttpStatusCode(), ex.getErrorCode()));
				}

			}

		} catch (Exception e) {
			// Output the stack trace.
			e.printStackTrace();
		}

		return "AtlantaBeverege AzureTabletoBlob ShopperTrackingData Completed Successfully";
	}

	public String abETL() {
		String command = "\"C:/Program Files (x86)/Microsoft SQL Server/130/DTS/Binn/DTExec.exe\" /FILE \"C:/CLOVERLEAF/ETL/JeanCoutuLabAzureBlob_Test.dtsx\"";
		String s = null;

        try {
            
	    // run the Unix "ps -ef" command
            // using the Runtime exec method:
            Process p = Runtime.getRuntime().exec(command);
            
            BufferedReader stdInput = new BufferedReader(new 
                 InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new 
                 InputStreamReader(p.getErrorStream()));

            // read the output from the command
            log.info("Here is the standard output of the command:\n");
            while ((s = stdInput.readLine()) != null) {
                log.info(s);
            }
            
            // read any errors from the attempted command
            log.info("Here is the standard error of the command (if any):\n");
            while ((s = stdError.readLine()) != null) {
                log.info(s);
            }
            return "success";
        }
        catch (IOException e) {
            log.info("exception happened - here's what I know: ");
            e.printStackTrace();
            return "error";
        }
		
	}

	public String abSqlToSpark(String tableName, String sql) {

		String url = "jdbc:sqlserver://devcloverleaf.database.windows.net:1433;database=CLF_DB;user=cloverleaf@devcloverleaf;password=Cl0verle@f!;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;";
		Connection connection = null;
		FileWriter fileWriter = null;

		final String storageConnectionStringDestination = "DefaultEndpointsProtocol=https;"
				+ "AccountName=cloverleafstoragespark;"
				+ "AccountKey=PENWlTyhPBOKErJ18hMxleVRXC+7X5pjQ0gEIBSddEBGSDW6riLk1uURtG9Re07yOGj9Eqs9EAp3WlxN/Wc7OQ==";

		try {

			// Delimiter used in CSV file
			final String TAB_DELIMITER = "\t";
			final String NEW_LINE_SEPARATOR = "\n";

			connection = DriverManager.getConnection(url);
			String schema = connection.getSchema();
			log.info("Successful connection - Schema: " + schema);

			log.info("Query data example:");
			log.info("=========================================");

			// Create and execute a SELECT SQL statement.
			String selectSql = "SELECT * from " + tableName;

			try (Statement statement = connection.createStatement();

					ResultSet resultSet = statement.executeQuery(selectSql)) {

				try {
					fileWriter = new FileWriter("C:/Users/Public/CloverLeaf Files/"+tableName + ".txt");

					while (resultSet.next()) {

						ResultSetMetaData rsmd = resultSet.getMetaData();
						int columnLength = rsmd.getColumnCount();

						for (int i = 1; i <= columnLength; i++) {
							fileWriter.append(String.valueOf(resultSet.getString(i)));
							if (i < columnLength) {
								fileWriter.append(TAB_DELIMITER);
							}
						}

						fileWriter.append(NEW_LINE_SEPARATOR);

					}

					log.info(" records fetched " + tableName);

					log.info(tableName + " Txt file was created successfully !!!");
				} catch (Exception e) {
					log.info("Error in TxtFileWriter !!!");
					e.printStackTrace();
				} finally {

					try {
						fileWriter.flush();
						fileWriter.close();

						File sourceFile = new File("C:/Users/Public/CloverLeaf Files/"+tableName+".txt");
						log.info("File Name : " + tableName + ".txt");

						CloudStorageAccount storageAccountDestination;
						CloudBlobClient blobClient = null;
						CloudBlobContainer container = null;

						// Parse the connection string and create a blob client to interact with Blob
						// storage
						storageAccountDestination = CloudStorageAccount.parse(storageConnectionStringDestination);
						blobClient = storageAccountDestination.createCloudBlobClient();
						container = blobClient.getContainerReference(
								"sparkcloverleaf-2017-12-14t11-57-47-334z/AtlantaBeveregeJeanCoutu/" + tableName); // Blob name

						// Getting a blob reference
						CloudBlockBlob blob = container
								.getBlockBlobReference(tableName + ".txt");

						// Creating blob and uploading file to it
						log.info("Uploading the " + tableName + " Txt file ");
						blob.uploadFromFile(sourceFile.getAbsolutePath());

					} catch (IOException e) {
						log.info("Error while flushing/closing fileWriter !!!");
						e.printStackTrace();
					}

					catch (StorageException ex) {
						log.info(
								String.format("Error returned from the service. Http code: %d and error code: %s",
										ex.getHttpStatusCode(), ex.getErrorCode()));
					} finally {
						// Assume that the arguments are in correct order //FROM BLOB TO SPARK CLUSTER
						String clusterName = "sparkcloverleaf";
						String clusterAdmin = "admin";
						String clusterPassword = "Cloverleaf@123";

						// Variables to hold statements, connection, and results
						Connection conn = null;
						Statement stmt = null;

						ResultSet res1 = null;

						try {

							// Create the connection string
							// Note that HDInsight always uses the external port 443 for SSL secure
							// connections, and will direct it to the hiveserver2 from there
							// to the internal port 10001 that Hive is listening on.
							String connectionQuery = String.format(
									"jdbc:hive2://%s.azurehdinsight.net:443/default;transportMode=http;ssl=true;httpPath=/hive2",
									clusterName);

							// Get the connection using the cluster admin user and password
							conn = DriverManager.getConnection(connectionQuery, clusterAdmin, clusterPassword);
							stmt = conn.createStatement();

							// Drop the 'hivesampletablederived' table, if it exists

							stmt.execute("drop table if exists " + tableName);

							// Create Database in Cluster
							stmt.execute("create database if not exists AtlantaBeveregeJeanCoutu");

							stmt.execute(sql);

							// Retrieve and display a description of the table
							sql = "desc AtlantaBeveregeJeanCoutu." + tableName;
							log.info("\nGetting a description of the table:");
							log.info(sql);
							res1 = stmt.executeQuery(sql);
							while (res1.next()) {
								log.info(res1.getString(1) + "\t" + res1.getString(2));
							}

						}

						// Catch exceptions
						catch (SQLException e) {
							e.getMessage();
							e.printStackTrace();
							System.exit(1);
						} catch (Exception ex) {
							ex.getMessage();
							ex.printStackTrace();
							System.exit(1);
						}
						// Close connections
						finally {
							if (res1 != null)
								res1.close();

							if (stmt != null)
								stmt.close();

						}

					}
				}
				connection.close();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "AtlantaBeverege SqlToSpark Completed Successfully for " + tableName;
	}

	public String abPowerBI() {
		return "https://app.powerbi.com/groups/03311bf6-6530-452a-9729-7e0e45b985a1/reports/c88bc4e2-4377-4d2e-8169-70b10b148e04/ReportSection6ac7401d5f51a2ba7e99";
	}

	public String jcTableToBlobEmotionTrackingData() {
		try {
			final String storageConnectionStringSource = "DefaultEndpointsProtocol=http;"
					+ "AccountName=jeancoutustorage;"
					+ "AccountKey=BsXNLsu8UfMstJxHnmYbiEsMDLApRFAhff1q5qwlJuO6ZA3s1+pVbP0tmtPn8pXz9kCdUTC2Hn+so0vNRZvwmA==";

			final String storageConnectionStringDestination = "DefaultEndpointsProtocol=https;"
					+ "AccountName=cloverleafsparkstorage;"
					+ "AccountKey=VMphLcZkEOlxSdfB+0guQsYqmnAuN336OnA0IYXM9jixPuCrnZ9COKj6WU0QAkI4RbfzR3VeMxUFEH2m8pilSg==";

			// Delimiter used in CSV file
			final String COMMA_DELIMITER = ",";
			final String NEW_LINE_SEPARATOR = "\n";

			// CSV file header
			final String FILE_HEADER = "PartitionKey,RowKey,Timestamp,EventId,EventTimestamp,StartTime,EndTime,ActiveStates,Created,Modified,Value";

			// Retrieve storage account from connection-string.
			CloudStorageAccount storageAccountSource = CloudStorageAccount.parse(storageConnectionStringSource);

			// Create the table client.
			CloudTableClient tableClient = storageAccountSource.createCloudTableClient();

			// Create a cloud table object for the table.
			CloudTable cloudTable = tableClient.getTableReference("emotiontrackingdata");

			// Specify a partition query, using "Smith" as the partition key filter.
			TableQuery<EmotionTrackingData> partitionQuery = TableQuery.from(EmotionTrackingData.class);

			FileWriter fileWriter = null;
			JSONParser parser = new JSONParser();

			try {
				fileWriter = new FileWriter("EmotionTrackingData" + dateFormat.format(date) + ".csv");

				// Write the CSV file header
				fileWriter.append(FILE_HEADER.toString());

				// Add a new line separator after the header
				fileWriter.append(NEW_LINE_SEPARATOR);

				int count = 0;

				// Checking config file for flag

				Object obj = parser.parse(new FileReader("C:/Users/Public/CloverLeaf Files/jcTableToBlobEmotionTrackingDataconfig.json"));

				JSONObject jsonObject = (JSONObject) obj;
	            long flag = (Long) jsonObject.get("flag");
	            String dateStr = (String) jsonObject.get("timestamp");
	            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	            Date mydate = sdf.parse(dateStr);
	            log.info(mydate);

				if (flag == 1) {
					String endDate = dateStr;
					// Loop through the results, displaying information about the entity.
					for (EmotionTrackingData entity : cloudTable.execute(partitionQuery)) {
						if ((entity.getTimestamp()).after(mydate)) {
							log.info((count++) + "\t" + entity.getPartitionKey() + "\t" + entity.getRowKey()
									+ "\t" + sdf.format(entity.getTimestamp()) + "\t" + entity.getEventId() + "\t"
									+ entity.getEventTimestamp() + "\t" + entity.getStartTime() + "\t"
									+ entity.getEndTime() + "\t" + entity.getActiveStates() + "\t" + entity.getCreated()
									+ "\t" + entity.getModified() + "\t" + entity.getValue()

							);

							fileWriter.append(String.valueOf(entity.getPartitionKey()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getRowKey()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(sdf.format(entity.getTimestamp())));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getEventId()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getEventTimestamp()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getStartTime()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getEndTime()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(
									"\"" + (String.valueOf(entity.getActiveStates())).replace("\"", "'") + "\"");
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getCreated()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getModified()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append("\"" + String.valueOf(entity.getValue()).replace("\"", "'") + "\"");

							fileWriter.append(NEW_LINE_SEPARATOR);
						}
						if((entity.getTimestamp()).after(sdf.parse(endDate))) {
				    		endDate = sdf.format(entity.getTimestamp());
				    	}
				    	
		            }
				    FileWriter fileWriter1 = new FileWriter("C:/Users/Public/CloverLeaf Files/jcTableToBlobEmotionTrackingDataconfig.json");
		            JSONObject configObj = new JSONObject();
		            configObj.put("flag",1);
		            configObj.put("timestamp", endDate);
		            log.info("\n end date: " + endDate);
		            fileWriter1.write(configObj.toJSONString());
		            fileWriter1.close();
				}
				if (flag == 0) {
					String endDate = sdf.format(new Date());
					// Loop through the results, displaying information about the entity.
					for (EmotionTrackingData entity : cloudTable.execute(partitionQuery)) {

						log.info((count++) + "\t" + entity.getPartitionKey() + "\t" + entity.getRowKey()
								+ "\t" + sdf.format(entity.getTimestamp()) + "\t" + entity.getEventId() + "\t"
								+ entity.getEventTimestamp() + "\t" + entity.getStartTime() + "\t" + entity.getEndTime()
								+ "\t" + entity.getActiveStates() + "\t" + entity.getCreated() + "\t"
								+ entity.getModified() + "\t" + entity.getValue()

						);

						fileWriter.append(String.valueOf(entity.getPartitionKey()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getRowKey()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(sdf.format(entity.getTimestamp())));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getEventId()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getEventTimestamp()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getStartTime()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getEndTime()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append("\"" + (String.valueOf(entity.getActiveStates())).replace("\"", "'") + "\"");
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getCreated()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getModified()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append("\"" + String.valueOf(entity.getValue()).replace("\"", "'") + "\"");

						fileWriter.append(NEW_LINE_SEPARATOR);
						if(count == 1) {
							endDate = sdf.format(entity.getTimestamp());
						}
						else {
							if((entity.getTimestamp()).after(sdf.parse(endDate))) {
					    		endDate = sdf.format(entity.getTimestamp());
					    	}
						}
						
				    }
				    FileWriter fileWriter1 = new FileWriter("C:/Users/Public/CloverLeaf Files/jcTableToBlobEmotionTrackingDataconfig.json");
		            JSONObject configObj = new JSONObject();
		            configObj.put("flag",1);
		            configObj.put("timestamp", endDate);
		            log.info("\n end date: " + endDate);
		            fileWriter1.write(configObj.toJSONString());
		            fileWriter1.close();
				}

				log.info(count + " records fetched emotiontrackingdata");

				log.info("emotiontrackingdata CSV file was created successfully !!!");

			} catch (Exception e) {
				log.info("Error in CsvFileWriter !!!");
				e.printStackTrace();
			} finally {

				try {
					fileWriter.flush();
					fileWriter.close();

					File sourceFile = new File("C:/Users/Public/CloverLeaf Files/EmotionTrackingData" + dateFormat.format(date) + ".csv");
					log.info("File Name : EmotionTrackingData" + dateFormat.format(date) + ".csv");

					CloudStorageAccount storageAccountDestination;
					CloudBlobClient blobClient = null;
					CloudBlobContainer container = null;

					// Parse the connection string and create a blob client to interact with Blob
					// storage
					storageAccountDestination = CloudStorageAccount.parse(storageConnectionStringDestination);
					blobClient = storageAccountDestination.createCloudBlobClient();
					container = blobClient.getContainerReference(
							"clfspark-2017-11-08t09-26-18-871z/JeanCoutuSorage/EmotionTrackingData"); // Blob Name

					// Getting a blob reference
					CloudBlockBlob blob = container
							.getBlockBlobReference("EmotionTrackingData" + dateFormat.format(date) + ".csv");

					// Creating blob and uploading file to it
					log.info("Uploading the EmotionTrackingData CSV file ");
					blob.uploadFromFile(sourceFile.getAbsolutePath());

					// //Listing contents of container
					// for (ListBlobItem blobItem : container.listBlobs()) {
					// log.info("URI of blob is: " + blobItem.getUri());
					// }

				} catch (IOException e) {
					log.info("Error while flushing/closing fileWriter !!!");
					e.printStackTrace();
				} catch (StorageException ex) {
					System.out
							.println(String.format("Error returned from the service. Http code: %d and error code: %s",
									ex.getHttpStatusCode(), ex.getErrorCode()));
				}
			}
		} catch (Exception e) {
			// Output the stack trace.
			e.printStackTrace();
		}

		return "JeanCoutu AzureTabletoBlob EmotionTrackingData Completed Successfully";
	}

	public String jcTableToBlobShopperTrackingData() {
		try {

			final String storageConnectionStringSource = "DefaultEndpointsProtocol=http;"
					+ "AccountName=jeancoutustorage;"
					+ "AccountKey=BsXNLsu8UfMstJxHnmYbiEsMDLApRFAhff1q5qwlJuO6ZA3s1+pVbP0tmtPn8pXz9kCdUTC2Hn+so0vNRZvwmA==";

			final String storageConnectionStringDestination = "DefaultEndpointsProtocol=https;"
					+ "AccountName=cloverleafsparkstorage;"
					+ "AccountKey=VMphLcZkEOlxSdfB+0guQsYqmnAuN336OnA0IYXM9jixPuCrnZ9COKj6WU0QAkI4RbfzR3VeMxUFEH2m8pilSg==";

			// Delimiter used in CSV file
			final String COMMA_DELIMITER = ",";
			final String NEW_LINE_SEPARATOR = "\n";

			// CSV file header
			final String FILE_HEADER = "PartitionKey,RowKey,Timestamp,EventId,SensorRegistryId,Value,Created,Modified";

			// Define constants for filters.
			final String PARTITION_KEY = "PartitionKey";
			final String ROW_KEY = "RowKey";
			final String TIMESTAMP = "Created";

			// Retrieve storage account from connection-string.
			CloudStorageAccount storageAccountSource = CloudStorageAccount.parse(storageConnectionStringSource);

			// Create the table client.
			CloudTableClient tableClient = storageAccountSource.createCloudTableClient();

			// Create a cloud table object for the table.
			CloudTable cloudTable = tableClient.getTableReference("shoppertrackingdata");

			String timeStampFilter = TableQuery.generateFilterCondition(TIMESTAMP, QueryComparisons.GREATER_THAN,
					new Date());

			String rowFilter = TableQuery.generateFilterCondition(ROW_KEY, QueryComparisons.GREATER_THAN,
					"00355a0c-a40a-49ba-872e-927a7ed24d02");

			String partitionFilter = TableQuery.generateFilterCondition(PARTITION_KEY, QueryComparisons.EQUAL,
					"ShopperTrackingEvent");

			String combinedFilter = TableQuery.combineFilters(partitionFilter, Operators.AND, rowFilter);

			// Specify a partition query, using "Smith" as the partition key filter.
			TableQuery<ShopperTrackingData> partitionQuery = TableQuery.from(ShopperTrackingData.class);

			FileWriter fileWriter = null;
			JSONParser parser = new JSONParser();

			try {
				fileWriter = new FileWriter("ShopperTrackingData" + dateFormat.format(date) + ".csv");

				// Write the CSV file header
				fileWriter.append(FILE_HEADER.toString());

				// Add a new line separator after the header
				fileWriter.append(NEW_LINE_SEPARATOR);

				int count = 0;
				

				// Checking config file for flag

				Object obj = parser.parse(new FileReader("C:/Users/Public/CloverLeaf Files/jcTableToBlobShopperTrackingDataconfig.json"));

				JSONObject jsonObject = (JSONObject) obj;
	            long flag = (Long) jsonObject.get("flag");
	            String dateStr = (String) jsonObject.get("timestamp");
	            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	            Date mydate = sdf.parse(dateStr);
	            log.info(mydate);
				if (flag == 1) {
					String endDate = dateStr;
					// Loop through the results, displaying information about the entity.
					for (ShopperTrackingData entity : cloudTable.execute(partitionQuery)) {
						if ((entity.getTimestamp()).after(mydate)) {
							log.info((count++) + "\t" + entity.getPartitionKey() + "\t" + entity.getRowKey()
									+ "\t" + sdf.format(entity.getTimestamp()) + "\t" + entity.getEventId() + "\t"
									+ entity.getSensorRegistryId() + "\t" + entity.getValue() + "\t"
									+ entity.getCreated() + "\t" + entity.getModified());

							fileWriter.append(String.valueOf(entity.getPartitionKey()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getRowKey()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(sdf.format(entity.getTimestamp())));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getEventId()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getSensorRegistryId()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append("\"" + (String.valueOf(entity.getValue())).replace("\"", "'") + "\"");
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getCreated()));
							fileWriter.append(COMMA_DELIMITER);
							fileWriter.append(String.valueOf(entity.getModified()));
							fileWriter.append(NEW_LINE_SEPARATOR);
						}
						if((entity.getTimestamp()).after(sdf.parse(endDate))) {
				    		endDate = sdf.format(entity.getTimestamp());
				    	}
				    	
		            }
				    FileWriter fileWriter1 = new FileWriter("C:/Users/Public/CloverLeaf Files/jcTableToBlobShopperTrackingDataconfig.json");
		            JSONObject configObj = new JSONObject();
		            configObj.put("flag",1);
		            configObj.put("timestamp", endDate);
		            log.info("\n end date: " + endDate);
		            fileWriter1.write(configObj.toJSONString());
		            fileWriter1.close();
				}

				if (flag == 0) {
					String endDate = sdf.format(new Date());
					// Loop through the results, displaying information about the entity.
					for (ShopperTrackingData entity : cloudTable.execute(partitionQuery)) {
						log.info((count++) + "\t" + entity.getPartitionKey() + "\t" + entity.getRowKey()
								+ "\t" + sdf.format(entity.getTimestamp()) + "\t" + entity.getEventId() + "\t"
								+ entity.getSensorRegistryId() + "\t" + entity.getValue() + "\t" + entity.getCreated()
								+ "\t" + entity.getModified()

						);

						fileWriter.append(String.valueOf(entity.getPartitionKey()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getRowKey()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(sdf.format(entity.getTimestamp())));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getEventId()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getSensorRegistryId()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append("\"" + (String.valueOf(entity.getValue())).replace("\"", "'") + "\"");
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getCreated()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(String.valueOf(entity.getModified()));
						fileWriter.append(NEW_LINE_SEPARATOR);
						if(count == 1) {
							endDate = sdf.format(entity.getTimestamp());
						}
						else {
							if((entity.getTimestamp()).after(sdf.parse(endDate))) {
					    		endDate = sdf.format(entity.getTimestamp());
					    	}
						}
						
				    }
				    FileWriter fileWriter1 = new FileWriter("C:/Users/Public/CloverLeaf Files/jcTableToBlobShopperTrackingDataconfig.json");
		            JSONObject configObj = new JSONObject();
		            configObj.put("flag",1);
		            configObj.put("timestamp", endDate);
		            log.info("\n end date: " + endDate);
		            fileWriter1.write(configObj.toJSONString());
		            fileWriter1.close();

				}

				log.info(count + " records fetched shoppertrackingdata");

				log.info("shoppertrackingdata CSV file was created successfully !!!");

			} catch (Exception e) {
				log.info("Error in CsvFileWriter !!!");
				e.printStackTrace();
			} finally {

				try {
					fileWriter.flush();
					fileWriter.close();

					File sourceFile = new File("C:/Users/Public/CloverLeaf Files/ShopperTrackingData" + dateFormat.format(date) + ".csv");
					log.info("File Name : ShopperTrackingData" + dateFormat.format(date) + ".csv");

					CloudStorageAccount storageAccountDestination;
					CloudBlobClient blobClient = null;
					CloudBlobContainer container = null;

					// Parse the connection string and create a blob client to interact with Blob
					// storage
					storageAccountDestination = CloudStorageAccount.parse(storageConnectionStringDestination);
					blobClient = storageAccountDestination.createCloudBlobClient();
					container = blobClient.getContainerReference(
							"clfspark-2017-11-08t09-26-18-871z/JeanCoutuSorage/ShopperTrackingData");

					// Getting a blob reference
					CloudBlockBlob blob = container
							.getBlockBlobReference("ShopperTrackingData" + dateFormat.format(date) + ".csv");

					// Creating blob and uploading file to it
					log.info("Uploading the ShopperTrackingData CSV file ");
					blob.uploadFromFile(sourceFile.getAbsolutePath());

					// //Listing contents of container
					// for (ListBlobItem blobItem : container.listBlobs()) {
					// log.info("URI of blob is: " + blobItem.getUri());
					// }

				} catch (IOException e) {
					log.info("Error while flushing/closing fileWriter !!!");
					e.printStackTrace();
				} catch (StorageException ex) {
					System.out
							.println(String.format("Error returned from the service. Http code: %d and error code: %s",
									ex.getHttpStatusCode(), ex.getErrorCode()));
				}

			}

		} catch (Exception e) {
			// Output the stack trace.
			e.printStackTrace();
		}
		return "JeanCoutu AzureTabletoBlob EmotionTrackingData Completed Successfully";
	}

	public String jcETL() {
		String command = "\"C:/Program Files (x86)/Microsoft SQL Server/130/DTS/Binn/DTExec.exe\" /FILE \"C:/CLOVERLEAF/ETL/JeanCoutuLabAzureBlob_Test.dtsx\"";
		String s = null;

        try {
            
	    // run the Unix "ps -ef" command
            // using the Runtime exec method:
            Process p = Runtime.getRuntime().exec(command);
            
            BufferedReader stdInput = new BufferedReader(new 
                 InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new 
                 InputStreamReader(p.getErrorStream()));

            // read the output from the command
            log.info("Here is the standard output of the command:\n");
            while ((s = stdInput.readLine()) != null) {
                log.info(s);
            }
            
            // read any errors from the attempted command
            log.info("Here is the standard error of the command (if any):\n");
            while ((s = stdError.readLine()) != null) {
                log.info(s);
            }
            return "success";
        }
        catch (IOException e) {
            log.info("exception happened - here's what I know: ");
            e.printStackTrace();
            return "error";
        }
	}

	public String jcSqlToSpark(String tableName, String sql) {

		// Connect to database
		String url = "jdbc:sqlserver://devcloverleaf.database.windows.net:1433;database=CLF_DB;user=cloverleaf@devcloverleaf;password=Cl0verle@f!;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;";
		Connection connection = null;

		FileWriter fileWriter = null;

		final String storageConnectionStringDestination = "DefaultEndpointsProtocol=https;"
				+ "AccountName=cloverleafstoragespark;"
				+ "AccountKey=PENWlTyhPBOKErJ18hMxleVRXC+7X5pjQ0gEIBSddEBGSDW6riLk1uURtG9Re07yOGj9Eqs9EAp3WlxN/Wc7OQ==";

		try {

			// Delimiter used in CSV file
			final String TAB_DELIMITER = "\t";
			final String NEW_LINE_SEPARATOR = "\n";

			connection = DriverManager.getConnection(url);
			String schema = connection.getSchema();
			log.info("Successful connection - Schema: " + schema);

			log.info("Query data example:");
			log.info("=========================================");

			// Create and execute a SELECT SQL statement.
			String selectSql = "SELECT * from " + tableName;

			try (Statement statement = connection.createStatement();

					ResultSet resultSet = statement.executeQuery(selectSql)) {

				try {
					fileWriter = new FileWriter(tableName + ".txt");

					while (resultSet.next()) {

						ResultSetMetaData rsmd = resultSet.getMetaData();
						int columnLength = rsmd.getColumnCount();

						for (int i = 1; i <= columnLength; i++) {
							fileWriter.append(String.valueOf(resultSet.getString(i)));
							if (i < columnLength) {
								fileWriter.append(TAB_DELIMITER);
							}
						}

						fileWriter.append(NEW_LINE_SEPARATOR);

					}

					log.info(" records fetched " + tableName);

					log.info(tableName + " Txt file was created successfully !!!");
				} catch (Exception e) {
					log.info("Error in TxtFileWriter !!!");
					e.printStackTrace();
				} finally {

					try {
						fileWriter.flush();
						fileWriter.close();

						File sourceFile = new File(".\\"+tableName+".txt");
						log.info("File Name : " + tableName + ".txt");

						CloudStorageAccount storageAccountDestination;
						CloudBlobClient blobClient = null;
						CloudBlobContainer container = null;

						// Parse the connection string and create a blob client to interact with Blob
						// storage
						storageAccountDestination = CloudStorageAccount.parse(storageConnectionStringDestination);
						blobClient = storageAccountDestination.createCloudBlobClient();
						container = blobClient.getContainerReference(
								"sparkcloverleaf-2017-12-14t11-57-47-334z/AtlantaBeveregeJeanCoutu/" + tableName); // Blob name

						// Getting a blob reference
						CloudBlockBlob blob = container
								.getBlockBlobReference(tableName + ".txt");

						// Creating blob and uploading file to it
						log.info("Uploading the " + tableName + " Txt file ");
						blob.uploadFromFile(sourceFile.getAbsolutePath());

					} catch (IOException e) {
						log.info("Error while flushing/closing fileWriter !!!");
						e.printStackTrace();
					}

					catch (StorageException ex) {
						log.info(
								String.format("Error returned from the service. Http code: %d and error code: %s",
										ex.getHttpStatusCode(), ex.getErrorCode()));
					} finally {
						// Assume that the arguments are in correct order //FROM BLOB TO SPARK CLUSTER
						String clusterName = "sparkcloverleaf";
						String clusterAdmin = "admin";
						String clusterPassword = "Cloverleaf@123";

						// Variables to hold statements, connection, and results
						Connection conn = null;
						Statement stmt = null;

						ResultSet res1 = null;

						try {
							// Load the HiveServer2 JDBC driver
							Class.forName("org.apache.hive.jdbc.HiveDriver");

							// Create the connection string
							// Note that HDInsight always uses the external port 443 for SSL secure
							// connections, and will direct it to the hiveserver2 from there
							// to the internal port 10001 that Hive is listening on.
							String connectionQuery = String.format(
									"jdbc:hive2://%s.azurehdinsight.net:443/default;transportMode=http;ssl=true;httpPath=/hive2",
									clusterName);

							// Get the connection using the cluster admin user and password
							conn = DriverManager.getConnection(connectionQuery, clusterAdmin, clusterPassword);
							stmt = conn.createStatement();

							// Drop the 'hivesampletablederived' table, if it exists

							stmt.execute("drop table if exists " + tableName);

							// Create Database in Cluster
							stmt.execute("create database if not exists AtlantaBeveregeJeanCoutu");

							stmt.execute(sql);

							// Retrieve and display a description of the table
							sql = "desc AtlantaBeveregeJeanCoutu." + tableName;
							log.info("\nGetting a description of the table:");
							res1 = stmt.executeQuery(sql);
							while (res1.next()) {
								log.info(res1.getString(1) + "\t" + res1.getString(2));
							}

						}

						// Catch exceptions
						catch (SQLException e) {
							e.getMessage();
							e.printStackTrace();
							System.exit(1);
						} catch (Exception ex) {
							ex.getMessage();
							ex.printStackTrace();
							System.exit(1);
						}
						// Close connections
						finally {
							if (res1 != null)
								res1.close();

							if (stmt != null)
								stmt.close();

						}

					}
				}
				connection.close();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "JeanCoutu SqlToSpark Completed Successfully for " + tableName;
	}

	public String jcPowerBI() {
		return "https://app.powerbi.com/groups/me/reports/9d4aab25-6c8d-4f4b-89fa-fc9845dbf74f/ReportSection";
	}

}