package com.cloverleaf;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;


@Controller
@SessionAttributes("name")
public class LoginController{

	private LoginService service = new LoginService();
	private ProcessService processService = new ProcessService();
	static Logger log = Logger.getLogger(LoginController.class.getName());
	
	@RequestMapping(value="/login.htm",method=RequestMethod.POST)
	public ModelAndView loginController(@RequestParam("name") String name,
			@RequestParam("password")String password){
		log.info( "Current Directory :  " + System.getProperty("user.dir"));
		ModelAndView mav=null;
		String fileName="";
		if(service.validateUser(name, password)){
			fileName="welcome";
			
		}else{
			fileName="error";
		}
		mav=new ModelAndView(fileName);
		return mav;
	}
	
	
	@ResponseBody
	@RequestMapping(value="/dashboard.htm",method=RequestMethod.POST)
	public String processController(@RequestParam("accountName") String accountName,
			@RequestParam("processName") String processName, Model model){
//		System.out.println("processName: "+processName +" accountName: "+accountName);
		log.info( "Current Directory :  " + System.getProperty("user.dir"));
		log.info("accountName : "+accountName+" processName : "+processName);
		String report = processService.getReport(accountName, processName);
		
		if(processName.equals("PowerBI Report")) {
			return processService.abPowerBI();
		}
		log.info("report : "+report);
		return report;
		
//		if(processName.equals("PowerBI Report")) {
//			ModelAndView mav = new ModelAndView("report_powerBI") ;
//			log.debug("result : "+report);
//			model.addAttribute("result", report);
//			return mav;
//		}
//		ModelAndView mav = new ModelAndView("reports") ;
//		log.debug("report : "+report);
//		model.addAttribute("result", report);
//		return mav;
	}
	
}